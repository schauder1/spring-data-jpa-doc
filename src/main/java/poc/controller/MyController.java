package poc.controller;

import poc.entity.CEntity;
import poc.entity.DEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;
import poc.service.MyService;

@RestController
public class MyController {

    @Autowired
    MyService myService;

    @GetMapping("/fonctionnementpardefauttexte")
    public RedirectView fonctionnementpardefauttexte() {
        readResult(myService.fonctionnementpardefauttexte(1));
        return new RedirectView("/index.html");
    }

    @GetMapping("/entityGraph")
    public RedirectView entityGraph() {
        readResult(myService.entityGraph(1));
        return new RedirectView("/index.html");
    }

    @GetMapping("/fonctionnementpardefautjpql")
    public RedirectView fonctionnementpardefautjpql() {
        readResult(myService.fonctionnementpardefautjpql(1));
        return new RedirectView("/index.html");
    }

    @GetMapping("/joinjpql")
    public RedirectView joinjpql() {
        readResult(myService.joinjpql(1));
        return new RedirectView("/index.html");
    }

    @GetMapping("/fetchjpql")
    public RedirectView fetchjpql() {
        readResult(myService.fetchjpql(1));
        return new RedirectView("/index.html");
    }

    private void readResult(CEntity c) {
        System.out.println(c.getLabel());
        System.out.println(c.getA().getLabel());
        System.out.println(c.getB().getLabel());
        for (DEntity dEntity : c.getdList()) {
            System.out.println(dEntity.getLabel());
            System.out.println(dEntity.getE().getLabel());
        }
    }
}
