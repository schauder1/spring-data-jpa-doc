package poc.repository;

import poc.entity.AEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ARepository extends JpaRepository<AEntity, Long> {
}
